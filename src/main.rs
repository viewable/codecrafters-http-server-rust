use std::{io::Write, net::TcpListener};

const HTTP_VERSION: &str = "HTTP/1.1";
const STATUS_OK: u16 = 200;

fn main() {
    // You can use print statements as follows for debugging, they'll be visible when running tests.
    eprintln!("Logs from your program will appear here!");

    let listener = TcpListener::bind("127.0.0.1:4221").unwrap();

    for stream in listener.incoming() {
        match stream {
            Ok(mut stream) => {
                eprintln!("accepted new connection");
                let response = format!("{HTTP_VERSION} {STATUS_OK} OK\r\n\r\n");
                stream.write_all(response.as_bytes()).unwrap();
            }
            Err(e) => {
                eprintln!("error: {}", e);
            }
        }
    }
}
